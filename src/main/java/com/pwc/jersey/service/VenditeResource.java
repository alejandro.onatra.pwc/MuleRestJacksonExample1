package com.pwc.jersey.service;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/vendite")
public class VenditeResource {
	
    @GET
    @Produces({MediaType.APPLICATION_JSON})  //add MediaType.APPLICATION_XML if you want XML as well (don't forget @XmlRootElement)
    public Vendite[] getPerson(){
        
        Vendite[] v = new Vendite[2];
        
        // Sale one
        Vendite sale1 = new Vendite();

        sale1.setCod_cassiera("");
        sale1.setCod_negozio("01001010101");
        sale1.setData_vendita("20140125");
        sale1.setImporto_totale("250");
        
        Articoli[] articles = new Articoli[2];        
        
        // Article one
        
        Articoli article1 = new Articoli();
        
        article1.setTipo("VENDITA");
        article1.setRdv("U46210");
        article1.setImporto("48");
        article1.setSku("89740830090483");
        
        articles[0] = article1;
        
        // Article two 
        
        Articoli article2 = new Articoli();
        
        article2.setTipo("VENDITA");
        article2.setRdv("U46210");
        article2.setImporto("48");
        article2.setSku("89740830090483");
        
        articles[1] = article2;
        
        sale1.setArticoli(articles);
        
        // Set sales
        v[0] = sale1;
        
        // Sale two
        
        Vendite sale2 = new Vendite();

        sale2.setCod_cassiera("");

        sale2.setCod_negozio("01001010101");

        sale2.setData_vendita("20140125");

        sale2.setImporto_totale("250");
        
        articles = new Articoli[2];
        
        // Article one
        
        Articoli article3 = new Articoli();
        
        article3.setTipo("VENDITA");
        article3.setRdv("U46210");
        article3.setImporto("48");
        article3.setSku("89740830090483");
        
        articles[0] = article3;
        
        sale2.setArticoli(articles);
        
        // Set sales
        v[1] = sale1;      
        
        System.out.println("REST call...");
        
        //return Response.ok().entity(p).build();
        return v;       
        
    }

}
