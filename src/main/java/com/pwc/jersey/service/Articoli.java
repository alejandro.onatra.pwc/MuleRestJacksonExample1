package com.pwc.jersey.service;

public class Articoli
{
    private String tipo;

    private String rdv;

    private String importo;

    private String sku;

    public String getTipo ()
    {
        return tipo;
    }

    public void setTipo (String tipo)
    {
        this.tipo = tipo;
    }

    public String getRdv ()
    {
        return rdv;
    }

    public void setRdv (String rdv)
    {
        this.rdv = rdv;
    }

    public String getImporto ()
    {
        return importo;
    }

    public void setImporto (String importo)
    {
        this.importo = importo;
    }

    public String getSku ()
    {
        return sku;
    }

    public void setSku (String sku)
    {
        this.sku = sku;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [tipo = "+tipo+", rdv = "+rdv+", importo = "+importo+", sku = "+sku+"]";
    }
}
