package com.pwc.jersey.service;

public class Vendite
{
    private String[] sartorie;

    private String cod_cassiera;

    private String cod_negozio;

    private String data_vendita;

    private String importo_totale;

    private Articoli[] articoli;

    public String[] getSartorie ()
    {
        return sartorie;
    }

    public void setSartorie (String[] sartorie)
    {
        this.sartorie = sartorie;
    }

    public String getCod_cassiera ()
    {
        return cod_cassiera;
    }

    public void setCod_cassiera (String cod_cassiera)
    {
        this.cod_cassiera = cod_cassiera;
    }

    public String getCod_negozio ()
    {
        return cod_negozio;
    }

    public void setCod_negozio (String cod_negozio)
    {
        this.cod_negozio = cod_negozio;
    }

    public String getData_vendita ()
    {
        return data_vendita;
    }

    public void setData_vendita (String data_vendita)
    {
        this.data_vendita = data_vendita;
    }

    public String getImporto_totale ()
    {
        return importo_totale;
    }

    public void setImporto_totale (String importo_totale)
    {
        this.importo_totale = importo_totale;
    }

    public Articoli[] getArticoli ()
    {
        return articoli;
    }

    public void setArticoli (Articoli[] articoli)
    {
        this.articoli = articoli;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [sartorie = "+sartorie+", cod_cassiera = "+cod_cassiera+", cod_negozio = "+cod_negozio+", data_vendita = "+data_vendita+", importo_totale = "+importo_totale+", articoli = "+articoli+"]";
    }
}
